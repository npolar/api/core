package core

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"testing"
)

// DRIVER TEMPLATE

const driverName = "testDriver"

type BufCloser struct{ *bytes.Buffer }

func NewBufCloser() *BufCloser      { return &BufCloser{&bytes.Buffer{}} }
func (wcb *BufCloser) Close() error { return nil }

type TestDriver struct{}

func (td TestDriver) Open(st StorageType, opts json.RawMessage) (Conn, error) {
	return TestConn{}, nil
}

type TestConn struct{}

func (tc TestConn) Create(i *Info, r io.Reader) error { return nil }
func (tc TestConn) Update(i *Info, r io.Reader) error { return nil }
func (tc TestConn) Delete(i *Info) error              { return nil }
func (tc TestConn) WriteInfo(i *Info) error           { return nil }
func (tc TestConn) ReadInfo(i *Info) error            { return nil }
func (tc TestConn) Formats() []string                 { return []string{"txt"} }
func (tc TestConn) Close() error                      { return nil }

func (tc TestConn) Read(i *Info, ctx context.Context) (io.ReadSeeker, error) {
	return bytes.NewReader(nil), nil
}
func (tc TestConn) NewWriteCloser(i *Info) (io.WriteCloser, error) {
	return NewBufCloser(), nil
}
func (tc TestConn) InfoList(i *Info, r *RequestOptions) (io.Reader, error) {
	return bytes.NewReader(nil), nil
}

// TESTS

func TestRegisterDriver(t *testing.T) {
	RegisterDriver(driverName, TestDriver{})
	if Drivers()[0] != driverName {
		t.Fatal("It should register a driver")
	}
}

func TestNewConn(t *testing.T) {
	c, err := NewConn(driverName, DocStorage, nil)
	if err != nil {
		t.Fatal("It should open a new connection.\n\t->", err)
	}
	if c.Formats()[0] != "txt" {
		t.Fatal("The connection should work.")
	}
	_, err = NewConn("wrong", DocStorage, nil)
	if err == nil {
		t.Fatal("It should return an error when the driver doesn't exist.")
	}
}
