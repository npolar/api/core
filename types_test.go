package core

import (
	"encoding/json"
	"fmt"
	"regexp"
	"testing"
	"time"
)

func TestTag(t *testing.T) {
	t.Run(
		"NewTag:Default",
		func(t *testing.T) {
			// Blank Tag
			tag := NewTag()
			for _, num := range tag {
				if num != 0 {
					t.Fatal(
						`Version numbers should be 0 when called without input`,
					)
				}
			}
		},
	)

	t.Run(
		"NewTag:Initialized",
		func(t *testing.T) {
			// Initialized Tag
			inputs := []uint{1, 5, 10}
			tag := NewTag(inputs...)
			for i, n := range inputs {
				if tag[i] != n {
					t.Fatal("The level numbers should match the input.")
				}
			}
		},
	)

	t.Run(
		"ToString",
		func(t *testing.T) {
			tag := NewTag(5, 4, 11)
			// String Representation
			vs := fmt.Sprintf("v%d.%d.%d", 5, 4, 11)
			if tag.String() != vs {
				t.Fatal("It should return a semver string.")
			}
		},
	)

	t.Run(
		"Increment-PatchLevels",
		func(t *testing.T) {
			tag := NewTag(4, 18, 1)
			// Increment PatchLevels
			increments := []PatchLevel{Major, Minor, Patch}
			for i, p := range increments {
				v := tag[i] + 1
				tag.Increment(p)
				if tag[i] != v {
					t.Fatal("It should increment the given lvl.")
				}
			}
		},
	)

	t.Run(
		"MarshalText",
		func(t *testing.T) {
			// Marshal json
			var marshalTest = struct {
				Version Tag
			}{NewTag(2, 1, 1)}
			match := []byte(`{"Version":"v2.1.1"}`)
			b, err := json.Marshal(marshalTest)
			if err != nil {
				t.Fatal("It should marshal without errors")
			}
			if string(b) != string(match) {
				t.Fatalf("It should look like %s, but got %s", match, b)
			}
		},
	)

	t.Run(
		"UnmarshalText",
		func(t *testing.T) {
			// Unmarshal json
			input := []byte(`{"Version":"v2.1.1"}`)
			var unmarshalTest = struct {
				Version Tag
			}{}
			err := json.Unmarshal(input, &unmarshalTest)
			if err != nil {
				t.Fatal("It should unmarshal json")
			}
			if unmarshalTest.Version[0] != 2 {
				t.Fatal("It should have the correct major version")
			}
		},
	)
}

func TestTagString(t *testing.T) {
	t.Run(
		"NewTagString:Default",
		func(t *testing.T) {
			// Blank Tag
			tag := NewTagString()
			if tag.String() != "v0.0.0" {
				t.Fatal(
					`Version tag should be v0.0.0 when called without input`,
				)
			}
		},
	)

	t.Run(
		"NewTagString:Initialized",
		func(t *testing.T) {
			// Initialized Tag
			inputs := []uint{1, 5, 10}
			tag := NewTagString(inputs...)
			if tag.String() != "v1.5.10" {
				t.Fatal("The level numbers should match the input.")
			}
		},
	)

	t.Run(
		"ToString",
		func(t *testing.T) {
			tag := NewTagString(5, 4, 11)
			if tag.String() != "v5.4.11" {
				t.Fatal("It should return a semver string.")
			}
		},
	)

	t.Run(
		"Increment-PatchLevels",
		func(t *testing.T) {
			tag := NewTagString(4, 18, 1)
			versionStrings := []string{
				"v5.0.0",
				"v5.1.0",
				"v5.1.1",
			}

			// Increment PatchLevels
			increments := []PatchLevel{Major, Minor, Patch}
			for i, p := range increments {
				tag.Increment(p)
				if tag.String() != versionStrings[i] {
					t.Fatal("It should increment the given lvl.")
				}
			}
		},
	)

	t.Run(
		"Increment Uninitialzed TagString",
		func(t *testing.T) {
			var tag TagString
			tag.Increment(Major)
			if tag.String() != "v1.0.0" {
				t.Fatal("It should use a v0.0.0 tag when not initialized.")
			}
		},
	)
}

func TestDefaultRequestOptions(t *testing.T) {
	opts := DefaultRequestOptions()
	if opts == nil {
		t.Fatal("It should not be nil")
	}
	if opts.Page[0] != 0 && opts.Page[1] != 50 {
		t.Fatal("Page should start at 0 and be 50 in size")
	}
	if opts.Type != PageResponse {
		t.Fatal("It should return a page response by default")
	}
}

func TestInfo(t *testing.T) {
	uuidRxp := regexp.MustCompile(uuidPattern)

	// New Info
	var ifo = map[string]*Info{
		"Default":  NewInfo(),
		"Document": NewDocumentInfo(),
		"File":     NewFileInfo("ABC1"),
		"Tag":      NewTagInfo("ABC1"),
	}

	for l, i := range ifo {
		t.Run(
			fmt.Sprintf("New%sInfo", l),
			func(t *testing.T) {
				if i == nil {
					t.Fatal("The Info object should not be nil")
				}
				if (l != "Document" && l != "Default") && i.DocID != "ABC1" {
					t.Fatal("It should initialize with a document ID")
				}
				if !uuidRxp.MatchString(i.ID) {
					t.Fatal("The ID should be a UUIDv4")
				}
				if i.Owner == nil {
					t.Fatal("The Owner map should be initialized")
				}
				if i.Checksum == nil {
					t.Fatal("It should initialize the cheksum map")
				}
			},
		)
	}

	// Marshal Json
	t.Run(
		"MarshalJson",
		func(t *testing.T) {
			var i = &Info{}
			_, err := i.Json()
			if err != nil {
				t.Fatal("It shouldn't return an error")
			}
		},
	)

	// Unmarshal Json
	t.Run(
		"UnmarshalJson",
		func(t *testing.T) {
			var i = &Info{}
			var j = []byte(`{"docID":"Yolo"}`)
			err := i.UnmarshalJson(j)
			if err != nil {
				t.Fatal("It shouldn't return an error")
			}
			if i.DocID != "Yolo" {
				t.Fatal("It should set the DocID")
			}
		},
	)

	t.Run(
		"SetCreatedNow",
		func(t *testing.T) {
			var i = &Info{}
			err := i.SetCreatedNow()
			if err != nil {
				t.Fatal("It should set created without errors")
			}
			if i.Created.IsZero() {
				t.Fatal("Created shouldn't be zero")
			}
			err = i.SetCreatedNow()
			if err != errCreated {
				t.Fatal("It should throw an error when already set")
			}
			if !i.Modified.Equal(i.Created) {
				t.Fatal("It should set modified to the same time")
			}
		},
	)

	t.Run(
		"SetModifiedNow",
		func(t *testing.T) {
			var i = &Info{}
			i.SetModifiedNow()
			if i.Modified.IsZero() {
				t.Fatal("Modified shouldn't be zero")
			}
		},
	)

	t.Run(
		"SetDeletedNow",
		func(t *testing.T) {
			var i = &Info{}
			err := i.SetDeletedNow()
			if err != nil {
				t.Fatal("It should set deleted without errors")
			}
			if i.Deleted.IsZero() {
				t.Fatal("Deleted shouldn't be zero")
			}
			err = i.SetDeletedNow()
			if err != errDeleted {
				t.Fatal("It should throw an error when already set")
			}
		},
	)

	t.Run(
		"SetReleasedNow",
		func(t *testing.T) {
			var i = &Info{}
			err := i.SetReleasedNow()
			if err != nil {
				t.Fatal("It should set released without errors")
			}
			if i.Released.IsZero() {
				t.Fatal("Released shouldn't be zero")
			}
			err = i.SetReleasedNow()
			if err != errRelease {
				t.Fatal("It should throw an error when already set")
			}
		},
	)

	t.Run(
		"SetReleased",
		func(t *testing.T) {
			var i = &Info{}
			err := i.SetReleased("2018-08-07T15:29:00Z")
			if err != nil {
				t.Fatal("It should set released without errors")
			}
			if i.Released.IsZero() {
				t.Fatal("Released shouldn't be zero")
			}
			err = i.SetReleased("")
			if err == nil {
				t.Fatal("It should throw an error on wrong input")
			}
		},
	)

	t.Run(
		"IsReleased",
		func(t *testing.T) {
			var i = &Info{Released: time.Now().UTC()}
			if !i.IsReleased() {
				t.Fatal("It should be true when released")
			}
		},
	)

	t.Run(
		"IsDeleted",
		func(t *testing.T) {
			var i = &Info{Deleted: time.Now().UTC()}
			if !i.IsDeleted() {
				t.Fatal("It should be true when deleted")
			}
		},
	)
}
