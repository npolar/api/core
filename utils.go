package core

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"runtime"
	"time"
)

// UUIDv4 generates a UUID following version 4 of the UUID spec (random UUID)
func UUIDv4() (id string) {
	switch runtime.GOOS {
	case "linux", "darwin", "freebsd":
		id = urandomUUID()
	default:
		id = randUUID()
	}
	return id
}

// urandomUUID uses the /dev/urandom files in unix like operationg systems to
// generate a random character sequence for the version 4 UUID.
func urandomUUID() string {
	f, err := os.Open("/dev/urandom")
	if err != nil {
		log.Panic("[Core] Error generating random sequence.", err)
	}
	defer f.Close()

	// Allocate a byte slice to hold our random characters
	b := make([]byte, 16)
	f.Read(b)
	// Set version bit to UUID version 4
	b[6] = (b[6] & 0x0f) | (4 << 4)

	// Format the random bytes to look like a UUID
	return fmt.Sprintf("%x-%x-%x-%x-%x", b[:4], b[4:6], b[6:8], b[8:10], b[10:])
}

// randUUID generates the random sequence using math/rand and the time of
// operation for entropy. This allows uuid generation on systems like windows
// that don't have the /dev/urandom file.
func randUUID() string {
	// Generate random seed using the current UTC time in nanoseconds
	rand.Seed(time.Now().UTC().UnixNano())
	const dict = "abcdefghijklmnopqrstuvwxyz0123456789"
	var charDict = []byte(dict)
	// Allocate a byte slice to hold our random characters
	b := make([]byte, 16)
	for i := 0; i < len(b); i++ {
		b[i] = charDict[rand.Intn(len(charDict))]
	}
	// Set version bit to UUID version 4
	b[6] = (b[6] & 0x0f) | (4 << 4)

	// Format the random bytes to look like a UUID
	return fmt.Sprintf("%x-%x-%x-%x-%x", b[:4], b[4:6], b[6:8], b[8:10], b[10:])
}
