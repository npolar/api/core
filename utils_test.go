package core

import (
	"regexp"
	"testing"
)

const uuidPattern = `^[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[a-f0-9]{4}-[a-f0-9]{12}$`

var outputs = map[string]string{
	"UUIDv4":      UUIDv4(),
	"urandomUUID": urandomUUID(),
	"randUUID":    randUUID(),
}

func TestUUIDv4(t *testing.T) {
	var uuidrxp = regexp.MustCompile(uuidPattern)

	for k, f := range outputs {
		t.Run(
			k,
			func(t *testing.T) {
				id := f
				if id == "" {
					t.Fatal("It shouldn't be blank.")
				}
				if !uuidrxp.MatchString(id) {
					t.Fatal("It should be v4 UUID.")
				}
			},
		)
	}
}
