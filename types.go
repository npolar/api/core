// Package core provides some basic data types, storage interfaces and utilities
// to enable the creation of re-usable storage solutions for the
// [npolar/api](https://gitlab.com/npolar/api) project.
package core

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"time"
)

// StorageType is used to define what kind of storage is being used.
type StorageType uint8

// ResponseType is used to define the response format being used.
//	- PageResponse: A single data entity.
//	- FeedResponse: Multiple data items separated by a newline delimiter.
type ResponseType uint8

// PatchLevel is used to define semantic version levels.
//	- Major
//	- Minor
//	- Patch
type PatchLevel uint8

const (
	// MIME type defaults for the different data types.
	defaultDocType  = "application/json"
	defaultFileType = "application/octet-stream"
	defaultTagType  = "application/gzip"
	defaultDir      = "/"

	DocStorage StorageType = iota
	FileStorage

	TagStorage = FileStorage

	FeedResponse ResponseType = iota
	PageResponse

	Major PatchLevel = iota
	Minor
	Patch
)

// AccessRights is used to reflect user permissions in the system.
type AccessRights struct {
	Create bool `json:"create"`
	Read   bool `json:"read"`
	Update bool `json:"update"`
	Delete bool `json:"delete"`
}

// Tag defines 3 dimensional uint slice representing the semver of an item.
//	- Tag[0] => Major version.
//	- Tag[1] => Minor version.
//	- Tag[2] => Patch version.
type Tag [3]uint

// NewTag initializes a new version tag with the provided levels. Levels should
// be provided as a comma separated list. If no version levels are provided a
// v0.0.0 Tag will be generated.
//	- First argument  => Major
//	- Second argument => Minor
//	- Third argument  => Patch
func NewTag(levels ...uint) Tag {
	var t = Tag{0, 0, 0}
	for i, l := range levels {
		if i <= 2 {
			t[i] = l
		}
	}
	return t
}

// Increment the with the specified PatchLevel. Given a v1.2.11 tag the
// following will happen given each PatchLevel:
//	- Major => v2.0.0
//	- Minor => v1.3.0
//	- Patch => v1.2.12
func (t *Tag) Increment(lvl PatchLevel) {
	switch lvl {
	case Major:
		t[0]++
		t[1] = 0
		t[2] = 0
	case Minor:
		t[1]++
		t[2] = 0
	case Patch:
		t[2]++
	}
}

// String returns the semver representation (v1.1.1) of the tag.
func (t Tag) String() string {
	return fmt.Sprintf("v%d.%d.%d", t[0], t[1], t[2])
}

// MarshalText is used when converting a TAG to JSON/XML and will return
// the string representation instead of a int array.
func (t Tag) MarshalText() ([]byte, error) {
	return []byte(t.String()), nil
}

var tagRegexp = regexp.MustCompile(`^v(\d+).(\d+).(\d+)$`)

// UnmarshalText is used when converting from JSON/XML to a Tag object.
func (t *Tag) UnmarshalText(text []byte) (err error) {
	var lvl int
	for i, c := range tagRegexp.FindStringSubmatch(string(text))[1:] {
		lvl, err = strconv.Atoi(string(c))
		t[i] = uint(lvl)
	}
	return
}

// TagString is the string representation of a tag.
type TagString string

// NewTagString generates a new tag string with the provided levels. Levels
// should be provided as a comma separated list. If no version levels are
// provided a v0.0.0 Tag will be generated. @SEE Tag type for more info.
// - First argument  => Major
// - Second argument => Minor
// - Third argument  => Patch
func NewTagString(levels ...uint) TagString {
	var t = NewTag(levels...)
	return TagString(t.String())
}

// String casts the TagString as a regular golang string.
func (ts TagString) String() string {
	return string(ts)
}

// Increment the current TagString with the given PatchLevel.
func (ts *TagString) Increment(level PatchLevel) {
	var lvl int
	var t Tag
	// If the tag isn't initialized we use a new 0.0.0 Tag. Otherwise we decode
	// the current TagString and use that.
	if ts.String() == "" {
		t = Tag{0, 0, 0}
	} else {
		for i, c := range tagRegexp.FindStringSubmatch(ts.String())[1:] {
			lvl, _ = strconv.Atoi(string(c))
			t[i] = uint(lvl)
		}
	}
	t.Increment(level)
	*ts = TagString(t.String())
}

// Range is a two dimensional array that defines the boundaries of a response.
// Array values have the following logic:
//	- Range[0]:	Defines the start offset (Number of results skipped).
//	- Range[1]:	Number of items in the range. When -1 is provided all items are
//		returned. This is not recommended except for special cases.
type Range [2]int

// RequestOptions contains basic information for result representation.
type RequestOptions struct {
	Page    Range
	Type    ResponseType
	Verbose bool
}

// DefaultRequestOptions returns a default option set.
func DefaultRequestOptions() *RequestOptions {
	return &RequestOptions{
		Page: Range{0, 50},
		Type: PageResponse,
	}
}

// @TODO figure out error handling
//type ErrorState struct {
//	ID      string    `json:"errorID"`
//	Module  string    `json:"module"`
//	Context *Info     `json:"context"`
//	Type    ErrorType `json:"errorType"`
//	Message error     `json:"message"`
//}
//
//func (es *ErrorState) Error() string {
//	return fmt.Sprintf("%s - %s: %s", es.ID, es.Module, es.Message)
//}

// Info defines system level metadata for the management of storage objects. It
// can be compared to filesystem inode info. It contains the needed fields to
// describe documents, files and data releases. Field mappings for json and bson
// are provided. Bson mappings are present because the reference driver is using
// MongoDB which uses bson as it's internal storage format.
type Info struct {
	ID     string `json:"-" bson:"_id"`     // Info ID
	DocID  string `json:"docID"`            // JSON ID
	FileID string `json:"fileID,omitempty"` // File ID
	Name   string `json:"name,omitempty"`   // Filename
	Type   string `json:"type"`             // MIME type
	Dir    string `json:"dir,omitempty"`    // Virt dir

	Size int64 `json:"size,omitempty"` // Bytesize of the content

	// Extended is an interace field meant to take any additional metadata the
	// user might want to store.
	Extended interface{} `json:"extended,omitempty"`

	// Info provides some fields to track version tags. They can be used when
	// implementing logic for data releases.
	//	- LatestRelease: This is used to track the newest version associated
	//		with an	info state.
	//	- Version: This tag describes the version of the current Item. It's
	//		primary use is when creating data releases.
	LatestRelease TagString `json:"latestRelease,omitempty"`
	Version       TagString `json:"version,omitempty"`

	// Timestamps
	//	- Created  : Time the associated item was creation.
	//	- Modified : Time the acciciated item was last modified.
	//	- Deleted  : Time the associated item was deleted.
	//	- Released : Time the associated item becomes publically readable.
	Created  time.Time `json:"created,omitempty"`
	Modified time.Time `json:"modified,omitempty"`
	Deleted  time.Time `json:"deleted,omitempty"`
	Released time.Time `json:"released,omitempty"`

	// List of owners and access permissions.
	//	- key: user/group name
	//	- val: AccessRights
	Owner map[string]*AccessRights `json:"owner,omitempty"`

	// Map of content checksums.
	//	- key: algorithm
	//	- val: checksum
	Checksum map[string]string `json:"checksum,omitempty"`

	// List of validators to use when creating,updating
	Validators []string `json:"validators,omitempty"`
}

// NewInfo initializes an Info object and returns the reference. The new
// instance is created with the following values:
//	- ID    : New Random UUID for this instance.
//	- DocID : New Random document UUID.
//	- Type  : Default document MIME type "application/json".
//	- Dir   : Default directory "/".
// It also initializes the Owner and Checksum maps.
func NewInfo() *Info {
	return &Info{
		ID:       UUIDv4(),
		DocID:    UUIDv4(),
		Type:     defaultDocType,
		Dir:      defaultDir,
		Owner:    make(map[string]*AccessRights),
		Checksum: make(map[string]string),
	}
}

// NewDocumentInfo is an alias of NewInfo().
func NewDocumentInfo() *Info {
	return NewInfo()
}

// NewFileInfo initializes an Info object describing a file. Files are handled
// using an attachment logic. When generating an Info reference for a file we
// need to provide the document ID the file is attached to. The new instance
// is created using the following values:
//	- ID     : New Random UUID for this instance.
//	- DocID  : The id we provided as the function argument.
//	- FileID : New Random file UUID.
//	- Type   : Default file MIME type "application/octet-stream".
//	- Dir    : Default storage directory "/".
// It also initializes the Owner and Checksum maps.
func NewFileInfo(docID string) *Info {
	return &Info{
		ID:       UUIDv4(),
		DocID:    docID,
		FileID:   UUIDv4(),
		Type:     defaultFileType,
		Dir:      defaultDir,
		Owner:    make(map[string]*AccessRights),
		Checksum: make(map[string]string),
	}
}

// NewTagInfo initializes an Info object describing a release tag. Tags use the
// same initialization logic as files. The only difference is the type.
//	- Type : Default tag MIME type "application/gzip". Generally releases are
//	stored as tar.gz archives.
func NewTagInfo(docID string) *Info {
	return &Info{
		ID:       UUIDv4(),
		DocID:    docID,
		FileID:   UUIDv4(),
		Type:     defaultTagType,
		Dir:      defaultDir,
		Owner:    make(map[string]*AccessRights),
		Checksum: make(map[string]string),
	}
}

// Json encodes the current Info state to json representation.
func (i *Info) Json() ([]byte, error) {
	return json.Marshal(i)
}

// UnmarshalJson tries to decode a byte slice to the info object.
func (i *Info) UnmarshalJson(data []byte) error {
	return json.Unmarshal(data, i)
}

var (
	// Basic errors
	errCreated = errors.New("item is already flagged as created")
	errDeleted = errors.New("item is already flagged as deleted")
	errRelease = errors.New("item already has a release date")
)

// SetCreatedNow sets the created timestamp to the current UTC time with second
// precision. In addition it sets the modified timestamp to be the same value.
// If the created timestamp of the object has already been set it will return
// the errCreated.
func (i *Info) SetCreatedNow() (err error) {
	if i.Created.IsZero() {
		i.Created = time.Now().UTC()
		i.Modified = i.Created
	} else {
		err = errCreated
	}
	return err
}

// SetModifiedNow sets the modified timestamp to the current UTC time with
// second precision.
func (i *Info) SetModifiedNow() {
	i.Modified = time.Now().UTC()
}

// SetDeletedNow sets the deleted timestamp to the current UTC time with second
// precision. If content was already flagged as deleted it will return the
// errDeleted.
func (i *Info) SetDeletedNow() (err error) {
	if i.Deleted.IsZero() {
		i.Deleted = time.Now().UTC()
	} else {
		err = errDeleted
	}
	return err
}

// SetReleasedNow sets the released timestamp to the current UTC time with
// second precision if the released timestamp is zero. If you want to override
// a non-zero released timestamp use the SetReleased function. If the released
// timestamp isn't zero it will throw a errRelease.
func (i *Info) SetReleasedNow() (err error) {
	if i.Released.IsZero() {
		i.Released = time.Now().UTC()
	} else {
		err = errRelease
	}
	return err
}

// SetReleased takes a RFC3339 compliant date time string and uses it to
// set the released timestamp.
func (i *Info) SetReleased(rfc3339DateTime string) error {
	rt, err := time.Parse(time.RFC3339, rfc3339DateTime)
	if err == nil {
		i.Released = rt
	}
	return err
}

// IsReleased returns true if the released timestamp is in the past. This
// includes items which have a zero value for the released timestamp.
func (i *Info) IsReleased() bool {
	return time.Now().UTC().Equal(i.Released) ||
		time.Now().UTC().After(i.Released)
}

// IsDeleted returns true if the deleted timestamp isn't zero.
func (i *Info) IsDeleted() bool {
	return !i.Deleted.IsZero()
}
