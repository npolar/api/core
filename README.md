# Core [![Go Report Card](https://goreportcard.com/badge/gitlab.com/npolar/api/core)](https://goreportcard.com/report/gitlab.com/npolar/api/core)

This package exposes a set of types, interfaces and utilities accommodating
re-usable API, storage and search component implementation.

Full API documentation can be found at [GoDoc](https://godoc.org/gitlab.com/npolar/api/core)

|Branch|Pipeline|Coverage|
|---|---|---|
|[**Master**](https://gitlab.com/npolar/api/core/tree/master)|[![pipeline status](https://gitlab.com/npolar/api/core/badges/master/pipeline.svg)](https://gitlab.com/npolar/api/core/commits/master)|[![coverage report](https://gitlab.com/npolar/api/core/badges/master/coverage.svg)](https://gitlab.com/npolar/api/core/commits/master)|
|[**Develop**](https://gitlab.com/npolar/api/core/tree/develop)|[![pipeline status](https://gitlab.com/npolar/api/core/badges/develop/pipeline.svg)](https://gitlab.com/npolar/api/core/commits/develop)|[![coverage report](https://gitlab.com/npolar/api/core/badges/develop/coverage.svg)](https://gitlab.com/npolar/api/core/commits/develop)|

## Installation

For installation you can use go get:
```bash
go get gitlab.com/npolar/api/core
```

## Usage

#### Register a new driver
If you have written a driver that implements the storage driver interface you
can make it available for use by calling [```RegisterDriver()```](https://godoc.org/gitlab.com/npolar/api/core#RegisterDriver)
with a unique driver name and a valid driver instance.

```go
core.RegisterDriver("myDriver", MyDriver{})
```

#### Establishing a connection
If the name you've chosen hasn't been taken and your Driver implements the
the interface correctly registration should succeed without errors. Once this
is done you can now use your driver by connecting to it using the drivername.
See [```NewConn()```](https://godoc.org/gitlab.com/npolar/api/core#NewConn)
for more information.

```go
// Provide a driverName, the storageType the driver will be used to store and
// optionally some json with configuration options for your driver.
conn, err := core.NewConn("myDriver", core.DocStorage, nil)
```
