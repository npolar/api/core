package core

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"sync"
)

// Storage defines the methods required for storage operations.
type Storage interface {
	// Read an item from storage. This method takes two arguments:
	//	- *Info: An Info reference which describes the content we want to
	//		retreive. The specific information required depends on the driver
	//		implementation you are using. Common fields are:
	//			- DocID when retrieving json documents or file attachments.
	//			- FileID when retriving a file.
	//			- Name when retrieving content by name instead of ID.
	//			- Dir when retrieving content grouped in a directory.
	//		In some cases the driver might update the Info reference in order to
	//		return the full descriptive information to the caller.
	//	- context.Context: A context can be provided to signal to the backend
	//		when the caller is finished reading the content. This can be
	//		required by the backend in order to cleanup file references. Please
	//		consult the	driver documentation about the need for this. When not
	//		required you can use nil instead.
	// Response values are:
	//	- io.ReadSeeker: When a match is found for the item described by the
	//		provided Info reference an io.ReadSeeker is returned. This means you
	//		can read content not only from the beginning but also from an offset.
	//	- error: Describes any failures that might occur during item retrieval.
	Read(*Info, context.Context) (io.ReadSeeker, error)

	// Create a new item in storage. This method takes two arguments:
	//	- *Info: An Info reference describing the item being created. In some
	//		cases the driver might update the Info reference to provide a full
	//		description of the created item. Common fields to be updated are:
	//			- DocID the item was created with.
	//			- FileID the item was created with.
	//			- Name that was used when storing the item.
	//			- Dir the item was stored in.
	//			- Created date-time.
	//			- Modified date-time.
	//	- io.Reader: An object that implements the io.Reader interace we can use
	//		to read the data we are going to store.
	// In case of failures an error is returned.
	Create(*Info, io.Reader) error

	// Update an existing item in storage. This method takes two areguments:
	//	- *Info: An Info reference used to lookup the relevant item. Refer to
	//		Read documentation for an overview of relevant fields.
	//	- io.Reader an object that implements the io.Reader interface we can use
	//		to read the updated data we are going to store. The reader should
	//		always contain the full item. Partial updates aren't supporeted by
	//		this method	unless indicated by the specific driver implementation.
	// In case of failures an error is returned.
	Update(*Info, io.Reader) error

	// Delete an item from storage. This method takes a single argument:
	//	- *Info: An info reference describing the item we are trying to delete.
	//		In case of successful delete this reference will be updated to
	//		reflect	the deletion time.
	// In case of failres an error is returend.
	Delete(*Info) error

	// Provides a write closer that allows you to write data to storage.
	NewWriteCloser(*Info) (io.WriteCloser, error)

	// Methods for setting/getting system metadata
	WriteInfo(*Info) error // Write Info to storage.
	ReadInfo(*Info) error  // Read Info from storage.

	// Retrieve full info list for current scope.
	InfoList(*Info, *RequestOptions) (io.Reader, error)
}

// Conn defines the minimum interface that needs to be satisfied for something
// to be used as a storage connection.
type Conn interface {
	Storage

	Formats() []string
	Close() error
}

// Driver interface defines an Open method used to establish a new storage conn.
type Driver interface {
	Open(st StorageType, opts json.RawMessage) (Conn, error)
}

var (
	driverMu sync.RWMutex
	drivers  = make(map[string]Driver)
)

// RegisterDriver allows the caller to make a new storage driver available. It
// checks if the provided package implements the correct methods. It will panic
// if a driver is already registered with the provided name. Arguments:
//	- name : String that can be used to select the driver for use.
//	- d    : Object that implements the Driver interface.
func RegisterDriver(name string, d Driver) {
	driverMu.Lock()
	defer driverMu.Unlock()
	prefix := "RegisterDriver"
	if d == nil {
		panic(prefix + ": Provided driver is nil")
	}
	if _, dup := drivers[name]; dup {
		panic(prefix + ": Called with existing driver name -> " + name)
	}
	drivers[name] = d
}

// Drivers returns a sorted list of available Drivers.
func Drivers() (list []string) {
	driverMu.Lock()
	defer driverMu.Unlock()
	for name := range drivers {
		list = append(list, name)
	}
	sort.Strings(list)
	return
}

// NewConn allows the caller to establish a new connection to the specified
// Driver. Arguments:
//	- name: Name of Driver to use.
//	- st  : StorageType. Must be one of the defined StorageType constants.
//		- core.DocStorage
//		- core.FileStorage
//		- core.TagStorage
//	- opts: Json encoded configuration options for the specified driver.
func NewConn(name string, st StorageType, opts json.RawMessage) (Conn, error) {
	driverMu.Lock()
	defer driverMu.Unlock()
	d, available := drivers[name]
	if !available {
		return nil, fmt.Errorf(
			"NewConn: unknown driver %q (forgotten import?)",
			name,
		)
	}
	return d.Open(st, opts)
}
