// Package template provides templates for the interfaces defined in the core
// package. These templates can be copied to avoid some boilerplate coding
// when writing a new implementation.
package template

import (
	"context"
	"encoding/json"
	"io"

	"gitlab.com/npolar/api/core"
)

const (
	driverName = "storageTemplate"
)

// StorageDriver is an anchor for the Open method.
type StorageDriver struct{}

// Open should implement storage system configuration and connection logic.
func (d StorageDriver) Open(st core.StorageType, opts json.RawMessage) (core.Conn, error) {
	return Conn{st}, nil
}

// Conn can be used to store connection specific configuration. It exposes the
// Storage interface methods.
type Conn struct {
	storageType core.StorageType
}

// Create wraps the storage systems insert logic.
func (c Conn) Create(info *core.Info, r io.Reader) error {
	return nil
}

// Read wraps the storage systems read logic.
func (c Conn) Read(inof *core.Info, ctx context.Context) (io.ReadSeeker, error) {
	return nil, nil
}

// Update wraps the storage systems update logic.
func (c Conn) Update(info *core.Info, r io.Reader) error {
	return nil
}

// Delete wraps the storage systems delete logic.
func (c Conn) Delete(info *core.Info) error {
	return nil
}

// InfoList should return a list of items stored in the storage system.
func (c Conn) InfoList(info *core.Info, opts *core.RequestOptions) (io.Reader, error) {
	return nil, nil
}

// ReadInfo should implement core.Info read logic for the storage system.
func (c Conn) ReadInfo(info *core.Info) error {
	return nil
}

// WriteInfo should implement core.Info storage logic for the storage system.
func (c Conn) WriteInfo(info *core.Info) error {
	return nil
}

// NewWriteCloser should return a writable and closable storage item for the
// storage system.
func (c Conn) NewWriteCloser(info *core.Info) (io.WriteCloser, error) {
	return nil, nil
}

// Formats should return a string slice of supported data types for the storage.
// Data types should be described as MIME types.
func (c Conn) Formats() []string {
	return nil
}

// Close wraps connection close logic for the storage system (if relevant).
func (c Conn) Close() error {
	return nil
}

// init registers the storageDriver with the core.Conn package as a valid and
// available storage driver implementation.
func init() {
	core.RegisterDriver(driverName, StorageDriver{})
}
